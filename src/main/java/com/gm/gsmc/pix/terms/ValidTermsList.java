package com.gm.gsmc.pix.terms;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.gm.gsmc.pix.terms.jpa.model.TermsDocVersion;

/**
 * @author TZVZCY
 */
public class ValidTermsList {

    private List<TermsDocVersion> terms;
    private List<String> invalidHash;

    public ValidTermsList () {
        this.terms = new ArrayList<>();
        this.invalidHash = new ArrayList<>();
    }

    public ValidTermsList (List<TermsDocVersion> terms, List<String> invalidHash) {
        this.terms = terms;
        this.invalidHash = invalidHash;
    }

    public List<TermsDocVersion> getValidTerms() {
        return this.terms;
    }

    public List<String> getInvalidHashes() {
        return this.invalidHash;
    }

    public void addValidTerms(TermsDocVersion doc) {
        this.terms.add(doc);
    }

    public void addInvalidHash(String hash) {
        this.invalidHash.add(hash);
    }

    public List<String> getValidHashes() {
        if (this.terms == null) {
            return null;
        }
        return this.terms.stream().map(TermsDocVersion::getHash).collect(Collectors.toList());
    }

}
