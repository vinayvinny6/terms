package com.gm.gsmc.pix.terms.config;

import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import com.gm.gsmc.pix.general.s3.S3Properties;
import com.gm.gsmc.pix.general.s3.S3Service;
import com.gm.gsmc.pix.terms.client.LocalVehiclesClient;
import com.gm.gsmc.pix.terms.client.VehiclesClient;

/**
 * @author TZVZCY
 */
@Configuration
@Profile ("default")
@EnableConfigurationProperties ({ S3Properties.class })
public class LocalConfig {

    private static final Logger log = LoggerFactory.getLogger(LocalConfig.class);

    @Bean
    public S3Service getS3Service(S3Properties properties) throws URISyntaxException {
        log.info("Loading Local Config - S3");
        return new S3Service(properties.toServiceInfo());
    }

    @Bean
    @Primary
    public VehiclesClient getVehiclesClient() {
        log.info("Loading Local Config - Vehicles Client");
        return new LocalVehiclesClient();
    }
}
