package com.gm.gsmc.pix.terms.jpa.model;

/**
 * @author TZVZCY
 */
public enum TermsDecision {

    ACCEPT, REJECT
}
