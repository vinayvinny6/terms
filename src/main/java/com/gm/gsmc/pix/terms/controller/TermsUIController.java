package com.gm.gsmc.pix.terms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gm.gsmc.pix.model.terms.TermsType;
import com.gm.gsmc.pix.terms.TermsDecisionManager;
import com.gm.gsmc.pix.terms.TermsManager;

/**
 * @author TZVZCY
 */
@Controller
@RequestMapping ("/ui")
public class TermsUIController {

    @Autowired
    private TermsManager termsManager;

    @Autowired
    private TermsDecisionManager termsDecisionManager;

    @Value ("${CF_INSTANCE_INDEX:local}")
    private String environmentIndex;

    @GetMapping (value = "/")
    public String viewIndex(Model model) {
        return viewCountries(model);
    }

    @GetMapping (value = "/countries")
    public String viewCountries(Model model) {
        model.addAttribute("environmentIndex", this.environmentIndex);
        model.addAttribute("countries", this.termsManager.getCountryList());
        return "countries";
    }

    @GetMapping (value = "/country/{country}")
    public String viewCountry(Model model, @PathVariable (name = "country") String country) {
        model.addAttribute("environmentIndex", this.environmentIndex);
        model.addAttribute("country", country);
        model.addAttribute("docs", this.termsManager.getDocumentsInCountry(country));
        return "country";
    }

    @GetMapping (value = "/country/{country}/terms/{termsType}")
    public String viewCountryDocument(Model model, @PathVariable (name = "country") String country, @PathVariable (name = "termsType") TermsType termsType) {
        model.addAttribute("environmentIndex", this.environmentIndex);
        model.addAttribute("country", country);
        model.addAttribute("termsType", termsType);
        model.addAttribute("docs", this.termsManager.getDocumentsInCountry(country, termsType));
        return "countryType";
    }

    @GetMapping (value = "/country/{country}/language/{language}/vehicle/{vin}")
    public String viewVinDocuments(Model model, @PathVariable (name = "country") String country, @PathVariable (name = "language") String language, @PathVariable (name = "vin") String vin) {
        model.addAttribute("environmentIndex", this.environmentIndex);
        model.addAttribute("country", country);
        model.addAttribute("language", language);
        model.addAttribute("vin", vin);
        model.addAttribute("terms", this.termsDecisionManager.getActiveTermsForVin(vin, country, language));
        return "vinList";
    }

    @GetMapping (value = "/vin")
    public String viewVinDocumentsQuery(Model model, @RequestParam ("country") String country, @RequestParam ("language") String language, @RequestParam ("vin") String vin) {
        return viewVinDocuments(model, country, language, vin);
    }
}
