package com.gm.gsmc.pix.terms.jpa.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author TZVZCY
 */
@Entity
public class TermsDecisionRecord {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    private String vin;
    private Date decisionDate;
    @Enumerated (EnumType.STRING)
    private TermsDecision status;
    private String hash;

    public TermsDecisionRecord () {

    }

    public TermsDecisionRecord (String vin, String hash, Date decisionDate, TermsDecision status) {
        this.vin = vin;
        this.hash = hash;
        this.decisionDate = decisionDate;
        this.status = status;
    }

    public TermsDecisionRecord (String vin, String hash, TermsDecision status) {
        this(vin, hash, new Date(), status);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVin() {
        return this.vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Date getDecisionDate() {
        return this.decisionDate;
    }

    public void setDecisionDate(Date decisionDate) {
        this.decisionDate = decisionDate;
    }

    public TermsDecision getStatus() {
        return this.status;
    }

    public void setStatus(TermsDecision status) {
        this.status = status;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

}
