package com.gm.gsmc.pix.terms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gm.gsmc.pix.api.service.TermsService;
import com.gm.gsmc.pix.model.CountryIsoCode;
import com.gm.gsmc.pix.model.terms.Terms;
import com.gm.gsmc.pix.model.terms.TermsAcceptance;
import com.gm.gsmc.pix.model.terms.TermsRequest;
import com.gm.gsmc.pix.model.terms.TermsSent;
import com.gm.gsmc.pix.terms.TermsDecisionManager;
import com.gm.gsmc.pix.terms.TermsManager;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping ("/")
@Api ("Terms Service - Manages Terms and Conditions for NGI vehicles ")
public class TermsController implements TermsService {

    @Autowired
    private TermsManager termsManager;

    @Autowired
    private TermsDecisionManager termsDecisionManager;

    @Override
    @ApiOperation (value = "Get Active Terms (no vehicle)", notes = "Returns all valid, active Terms for a country and language")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The Country or language is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "/terms/active/{countryCode}/{language}", produces = { "application/json" })
    public List<Terms> getActiveTerms(
            @PathVariable ("countryCode") @ApiParam (defaultValue = "US", value = "The Country to get Terms for", example = "US,CA,MX,CN") String countryCode,
            @PathVariable ("language") @ApiParam (defaultValue = "en", value = "The Language to get Terms in", example = "en,fr,es") String language) {
        return this.termsManager.getActiveTerms(countryCode, language);
    }

    @Override
    @ApiOperation (value = "Get All Terms (no vehicle)", notes = "Returns all valid, active Terms for a country and language")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The Country or language is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "/terms/all/{countryCode}/{language}", produces = { "application/json" })
    public List<Terms> getAllTerms(
            @PathVariable ("countryCode") @ApiParam (defaultValue = "US", value = "The Country to get Terms for", example = "US,CA,MX,CN") String countryCode,
            @PathVariable ("language") @ApiParam (defaultValue = "en", value = "The Language to get Terms in", example = "en,fr,es") String language) {
        return this.termsManager.getAllTerms(countryCode, language);
    }

    @Override
    @ApiOperation (value = "Get All Terms for a vehicle", notes = "Returns all Terms (of any status) for a vehicle in a country and language")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN, Country, or Language is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "/terms/all/{countryCode}/{language}/{vin}", produces = { "application/json" })
    public TermsAcceptance getAllTermsForVin(
            @PathVariable ("countryCode") @ApiParam (defaultValue = "US", value = "The Country to get Terms for", example = "US,CA,MX,CN") String countryCode,
            @PathVariable ("language") @ApiParam (defaultValue = "en", value = "The Language to get Terms in", example = "en,fr,es") String language,
            @PathVariable ("vin") @ApiParam (value = "The vin of the vehicle to get terms for") String vin) {
        return this.termsDecisionManager.getAllTermsForVin(vin, countryCode, language);
    }

    @Override
    @ApiOperation (value = "Get Active Terms for a vehicle", notes = "Returns all active Terms for a vehicle in a country and language")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN, Country, or Language is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "/terms/active/{countryCode}/{language}/{vin}", produces = { "application/json" })
    public TermsAcceptance getActiveTermsForVin(
            @PathVariable ("countryCode") @ApiParam (defaultValue = "US", value = "The Country to get Terms for", example = "US,CA,MX,CN") String countryCode,
            @PathVariable ("language") @ApiParam (defaultValue = "en", value = "The Language to get Terms in", example = "en,fr,es") String language,
            @PathVariable ("vin") @ApiParam (value = "The vin of the vehicle to get terms for") String vin) {
        return this.termsDecisionManager.getActiveTermsForVin(vin, countryCode, language);
    }

    @Override
    @ApiOperation (value = "Get One Terms", notes = "Returns a single Terms based on its hash value")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The hash value is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "/terms/{hash}", produces = { "application/json" })
    public Terms getTerms(@PathVariable ("hash") @ApiParam (value = "The hash of the terms to get") String hash) {
        return this.termsManager.getTerms(hash);
    }

    @Override
    @ApiOperation (value = "Accept Terms", notes = "Accept a set of terms for a VIN")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN is not valid.  Invalid terms are handled successfully as a 200."),
            @ApiResponse (code = 500, message = "Internal system error") })
    @PostMapping (value = "/terms/accept", produces = { "application/json" })
    public TermsAcceptance acceptTerms(@RequestBody @ApiParam (value = "The request of which terms to accept", required = true) TermsRequest request) {
        return this.termsDecisionManager.acceptTerms(request);
    }

    @Override
    @ApiOperation (value = "Reject Terms", notes = "Reject a set of terms for a VIN")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN is not valid.  Invalid terms are handled successfully as a 200."),
            @ApiResponse (code = 500, message = "Internal system error") })
    @PostMapping (value = "/terms/reject", produces = { "application/json" })
    public TermsAcceptance rejectTerms(@RequestBody @ApiParam (value = "The request of which terms to reject", required = true) TermsRequest request) {
        return this.termsDecisionManager.rejectTerms(request);
    }

    @Override
    @ApiOperation (value = "Email Terms", notes = "Email a set of terms for a country and language")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response, OK"),
            @ApiResponse (code = 400, message = "The Email, Country, or Language is not valid."),
            @ApiResponse (code = 500, message = "Internal system error") })
    @PostMapping (value = "/terms/email", produces = { "application/json" })
    public TermsSent emailTerms(@RequestParam ("email") @ApiParam (value = "The email address to send terms and conditions to", required = true) String emailAddress,
            @RequestParam ("countryCode") @ApiParam (defaultValue = "US", value = "The Country to get Terms for", example = "US,CA,MX,CN", required = true) String countryCode,
            @RequestParam ("language") @ApiParam (defaultValue = "en", value = "The Language to get Terms in", example = "en,fr,es", required = true) String language,
            @RequestBody @ApiParam (value = "The request of which terms to email", required = true) TermsRequest request) {
        return this.termsManager.emailTerms(emailAddress, countryCode, language, request);
    }

    @Override
    @ApiOperation (value = "Reset Terms", notes = "Resets all terms accepted and rejected for a VIN")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The Email, Country, or Language is not valid."),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "/terms/reset/{vin}")
    public Boolean resetTerms(@PathVariable ("vin") @ApiParam (value = "The vin of the vehicle to reset terms for") String vin) {
        return this.termsDecisionManager.resetTerms(vin);
    }

    @Override
    @ApiOperation (value = "Get Country For Vehicle", notes = "Gets the Country the Vehicle is in, otherwise null")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The vin is not valid."),
            @ApiResponse (code = 500, message = "Internal system error") })
    @GetMapping (value = "/terms/vehicle/country/{vin}", produces = { "application/json" })
    public CountryIsoCode getCountryForVehicle(@PathVariable ("vin") @ApiParam (value = "The vin of the vehicle to get country for") String vin) {
        return this.termsDecisionManager.getCountryForVehicle(vin);
    }
}
