package com.gm.gsmc.pix.terms.jpa;

import org.springframework.data.repository.CrudRepository;

import com.gm.gsmc.pix.terms.jpa.model.TermsFile;

/**
 * @author TZVZCY
 */
public interface TermsFileRepository extends CrudRepository<TermsFile, Long> {

    public TermsFile findOneByIdAndHash(Long id, String hash);

}
