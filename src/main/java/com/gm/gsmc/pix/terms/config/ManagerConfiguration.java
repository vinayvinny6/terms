package com.gm.gsmc.pix.terms.config;

import org.springframework.context.annotation.Configuration;

/**
 * @author TZVZCY
 */
@Configuration
public class ManagerConfiguration {

    // This could be an alternative to annotating the manager class as an @Service
//    @Bean
//    public TermsManager getTermsManager() {
//        return new TermsManager();
//    }

}
