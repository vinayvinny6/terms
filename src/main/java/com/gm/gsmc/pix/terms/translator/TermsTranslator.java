package com.gm.gsmc.pix.terms.translator;

import org.springframework.stereotype.Service;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.terms.Terms;
import com.gm.gsmc.pix.terms.jpa.model.TermsDoc;
import com.gm.gsmc.pix.terms.jpa.model.TermsDocVersion;

/**
 * @author TZVZCY
 */
@Service
public class TermsTranslator extends AbstractModelTranslator<TermsDocVersion, Terms> {

    @Override
    public Terms translate(TermsDocVersion docVersion) {
        TermsDoc termsDoc = docVersion.getTermsDoc();

        Terms terms = new Terms();
        terms.setCountry(termsDoc.getCountry());
        terms.setDisplayName(termsDoc.getDisplayName());
        terms.setEffectiveDate(docVersion.getEffectiveDate());
        terms.setHash(docVersion.getHash());
        terms.setLanguage(termsDoc.getLanguage());
        terms.setOwner(termsDoc.getOwner());
        terms.setStatus(docVersion.getStatus());
        terms.setType(termsDoc.getType());
        terms.setUrl(docVersion.getUrl());
        terms.setVersion(docVersion.getVersion());
        return terms;
    }

}
