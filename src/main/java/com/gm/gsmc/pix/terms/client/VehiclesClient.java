package com.gm.gsmc.pix.terms.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Primary;

import com.gm.gsmc.pix.api.service.VehiclesService;

/**
 * @author TZVZCY - Zac Grantham
 */
@FeignClient (name = "vehicles", fallback = VehiclesClientFallback.class)
@Primary
public interface VehiclesClient extends VehiclesService {

}
