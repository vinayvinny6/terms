package com.gm.gsmc.pix.terms;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.gm.gsmc.pix.model.terms.Terms;
import com.gm.gsmc.pix.model.terms.TermsStatus;
import com.gm.gsmc.pix.terms.jpa.TermsDocVersionRepository;
import com.gm.gsmc.pix.terms.jpa.model.TermsDocVersion;
import com.gm.gsmc.pix.terms.translator.TermsTranslator;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author TZVZCY
 */
@Service
public class TermsCache {

    private static final Logger log = LoggerFactory.getLogger(TermsCache.class);

    // TODO Decide which of these need to exist and how to cache them
    // TODO Create ConfigurableCaffeineCacheManager

    @Autowired
    private TermsDocVersionRepository termsVersionRepo;

    @Autowired
    private TermsTranslator termsTranslator;

    @Cacheable ("allTermsDocs")
    public List<TermsDocVersion> getAllTermsDocs() {
        return Lists.newArrayList(this.termsVersionRepo.findAll());
    }

    @Cacheable ("activeTermsDocs")
    public List<TermsDocVersion> getActiveTermsDocs() {
        return Lists.newArrayList(this.termsVersionRepo.findAllByStatus(TermsStatus.ACTIVE));
    }

    @Cacheable ("allTermsHashes")
    public Map<String, TermsDocVersion> getAllTermsHashes() {
        // return getActiveTermsDocs().stream().collect(Collectors.toMap(TermsDocVersion::getHash, Function.identity()));
        return Maps.uniqueIndex(getAllTermsDocs(), TermsDocVersion::getHash);
    }

    @Cacheable ("activeTermsHashes")
    public Map<String, TermsDocVersion> getActiveTermsHashes() {
        // return getActiveTermsDocs().stream().collect(Collectors.toMap(TermsDocVersion::getHash, Function.identity()));
        return Maps.uniqueIndex(getActiveTermsDocs(), TermsDocVersion::getHash);
    }

    @Cacheable ("countryAllTermsDocs")
    public List<TermsDocVersion> getAllCountryTermsDocs(String country, String language) {
        return Lists.newArrayList(this.termsVersionRepo.findAllByTermsDocCountryAndTermsDocLanguage(country, language));
    }

    @Cacheable ("countryActiveTermsDocs")
    public List<TermsDocVersion> getActiveCountryTermsDocs(String country, String language) {
        return Lists.newArrayList(this.termsVersionRepo.getActiveVersions(country, language));
    }

    public Map<String, TermsDocVersion> getAllCountryTermsHashes(String country, String language) {
        return Maps.uniqueIndex(getAllCountryTermsDocs(country, language), TermsDocVersion::getHash);
    }

    public Map<String, TermsDocVersion> getActiveCountryTermsHashes(String country, String language) {
        return Maps.uniqueIndex(getActiveCountryTermsDocs(country, language), TermsDocVersion::getHash);
    }

    @Cacheable ("allTerms")
    public List<Terms> getAllTerms() {
        return this.termsTranslator.translate(getAllTermsDocs());
    }

    @Cacheable ("activeTerms")
    public List<Terms> getActiveTerms() {
        return this.termsTranslator.translate(getActiveTermsDocs());
    }

    @Cacheable ("countryAllTerms")
    public List<Terms> getAllCountryTerms(String country, String language) {
        return this.termsTranslator.translate(getAllCountryTermsDocs(country, language));
    }

    @Cacheable ("countryActiveTerms")
    public List<Terms> getActiveCountryTerms(String country, String language) {
        return this.termsTranslator.translate(getActiveCountryTermsDocs(country, language));
    }

    @CacheEvict (allEntries = true,
            cacheNames = { "allTermsDocs", "activeTermsDocs", "countryAllTermsDocs", "countryActiveTermsDocs", "allTerms", "activeTerms", "countryAllTerms", "countryActiveTerms",
                    "activeTermsHashes" })
    public void clearCache() {
        log.info("Cache Cleared");
    }
}
