package com.gm.gsmc.pix.terms.config;

import org.springframework.context.annotation.Configuration;

import com.gm.gsmc.pix.general.config.AbstractSwaggerBasicConfiguration;

/**
 * @author TZVZCY
 */
@Configuration
public class SwaggerConfiguration extends AbstractSwaggerBasicConfiguration {

    @Override
    protected String getName() {
        return "Terms";
    }

    @Override
    protected String getDescription() {
        return "Terms Service";
    }

    @Override
    protected String getControllerBasePackage() {
        return "com.gm.gsmc.pix.terms.controller";
    }

}
