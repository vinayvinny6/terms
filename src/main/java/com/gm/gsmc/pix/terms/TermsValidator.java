package com.gm.gsmc.pix.terms;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gm.gsmc.pix.terms.jpa.model.TermsDocVersion;

/**
 * Takes a list of terms hashes and determines which ones exist and which don't.
 * @author TZVZCY
 */
@Service
public class TermsValidator {

    @Autowired
    private TermsCache termsCache;

    public ValidTermsList getValidAllTerms(List<String> hashes) {
        Map<String, TermsDocVersion> hashMap = this.termsCache.getAllTermsHashes();
        ValidTermsList termsList = new ValidTermsList();
        hashes.stream().distinct().forEach(hash -> {
            if (hashMap.containsKey(hash)) {
                termsList.addValidTerms(hashMap.get(hash));
            } else {
                termsList.addInvalidHash(hash);
            }
        });
        return termsList;
    }

    public ValidTermsList getValidActiveTerms(List<String> hashes) {
        Map<String, TermsDocVersion> hashMap = this.termsCache.getActiveTermsHashes();
        ValidTermsList termsList = new ValidTermsList();
        hashes.stream().distinct().forEach(hash -> {
            if (hashMap.containsKey(hash)) {
                termsList.addValidTerms(hashMap.get(hash));
            } else {
                termsList.addInvalidHash(hash);
            }
        });
        return termsList;
    }

}
