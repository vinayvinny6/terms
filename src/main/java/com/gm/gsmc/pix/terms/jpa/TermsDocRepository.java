package com.gm.gsmc.pix.terms.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gm.gsmc.pix.model.terms.TermsType;
import com.gm.gsmc.pix.terms.jpa.model.TermsDoc;

/**
 * @author TZVZCY
 */
public interface TermsDocRepository extends CrudRepository<TermsDoc, Long> {

    TermsDoc findOneByCountryAndLanguageAndType(String country, String language, TermsType type);

    @Query ("select DISTINCT(td.country) from TermsDoc td")
    List<String> getCountries();

    @Query ("select DISTINCT(td.type) from TermsDoc td where td.country = :country")
    List<String> getDocumentsInCountry(@Param ("country") String country);

}
