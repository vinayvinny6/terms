package com.gm.gsmc.pix.terms.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;

import com.gm.gsmc.pix.general.controller.AbstractRestReponseExceptionHandler;

/**
 * @author TZVZCY
 */
@ControllerAdvice
public class PixExceptionHandler extends AbstractRestReponseExceptionHandler {

}
