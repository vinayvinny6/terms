package com.gm.gsmc.pix.terms.controller;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.emc.object.s3.bean.GetObjectResult;
import com.gm.gsmc.pix.terms.TermsFileManager;

import io.swagger.annotations.Api;

/**
 * @author TZVZCY
 */
@RestController
@RequestMapping ("/file")
@Api ("Terms File Download Service - Download Terms and Conditions files")
public class TermsFileController {

    @Autowired
    private TermsFileManager termsFileManager;

//    @GetMapping (value = "/download/{id}/{hash}/{fileName}", produces = {"application/pdf","text/html","application/octet-stream"})
//    public ResponseEntity<byte[]> getAppImage(@PathVariable (name = "termsId") String termsName) throws IOException {
//        S3Object obj = this.termsFileManager.readFile(termsName);
//
//        HttpHeaders headers = new HttpHeaders();
//        byte[] media = Streams.readAll(obj.getObjectContent());
//        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
//
//        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
//        return responseEntity;
//
//        InputStreamResource inputStreamResource = new InputStreamResource(obj.getObjectContent());
//        headers.setContentLength(obj.getObjectMetadata().getContentLength());
//
//        return ResponseEntity.ok().contentLength(obj.getObjectMetadata().getContentLength())
//                .contentType(MediaType.parseMediaType(obj.getObjectMetadata().getContentType()))
//                .body(new InputStreamResource(obj.getObjectContent()));
//        return new ResponseEntity(inputStreamResource, httpHeaders, HttpStatus.OK);
//    }

    @GetMapping (value = "/download/{id}/{hash}/{fileName}", produces = { "application/pdf", "text/html", "application/octet-stream" })
    public ResponseEntity<InputStreamResource> getAppImage(@PathVariable (name = "id") Long id, @PathVariable (name = "hash") String hash, @PathVariable (name = "fileName") String fileName) {
        GetObjectResult<InputStream> obj = this.termsFileManager.readFile(id, hash);

        return ResponseEntity.ok().contentLength(obj.getObjectMetadata().getContentLength())
                .contentType(MediaType.parseMediaType(obj.getObjectMetadata().getContentType()))
                .body(new InputStreamResource(obj.getObject()));
    }

//    @DeleteMapping (value = "/terms/pdf/{termsId}")
//    public String deleteAppImage(@PathVariable (name = "termsId") String termsName) {
//        this.termsFileManager.deleteFile(termsName);
//        return "OK";
//    }
}
