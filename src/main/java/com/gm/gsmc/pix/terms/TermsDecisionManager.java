package com.gm.gsmc.pix.terms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gm.gsmc.pix.model.CountryIsoCode;
import com.gm.gsmc.pix.model.terms.TermsAcceptance;
import com.gm.gsmc.pix.model.terms.TermsRequest;
import com.gm.gsmc.pix.terms.client.VehiclesClient;
import com.gm.gsmc.pix.terms.exception.TermsException;
import com.gm.gsmc.pix.terms.exception.TermsExceptionCode;
import com.gm.gsmc.pix.terms.exception.TermsInvalidInputException;
import com.gm.gsmc.pix.terms.jpa.TermsDecisionRecordRepository;
import com.gm.gsmc.pix.terms.jpa.model.TermsDecision;
import com.gm.gsmc.pix.terms.jpa.model.TermsDecisionRecord;
import com.gm.gsmc.pix.terms.jpa.model.TermsDocVersion;
import com.gm.gsmc.pix.terms.translator.TermsTranslator;

/**
 * @author TZVZCY
 */
@Service
@Transactional
public class TermsDecisionManager {

    @Autowired
    private TermsValidator termsValidator;

    @Autowired
    private TermsTranslator termsTranslator;

    @Autowired
    private TermsDecisionRecordRepository termsDecisionRepo;

    @Autowired
    private TermsCache termsCache;

    @Autowired
    private VehiclesClient vehiclesClient;

    public TermsAcceptance acceptTerms(TermsRequest request) {
        return actionTerms(request, TermsDecision.ACCEPT);
    }

    public TermsAcceptance rejectTerms(TermsRequest request) {
        return actionTerms(request, TermsDecision.REJECT);
    }

    public boolean resetTerms(String vin) {
        this.termsDecisionRepo.deleteByVin(vin);
        return true;
    }

    public TermsAcceptance getAllTermsForVin(String vin, String countryCode, String language) {
        if (vin == null) {
            throw new TermsInvalidInputException("Request missing vin");
        }
        if (countryCode == null) {
            throw new TermsInvalidInputException("Request missing country code");
        }
        if (language == null) {
            throw new TermsInvalidInputException("Request missing language");
        }
        Map<String, TermsDocVersion> versions = this.termsCache.getAllCountryTermsHashes(countryCode, language);
        return findDecisions(vin, countryCode, versions);
    }

    public TermsAcceptance getActiveTermsForVin(String vin, String countryCode, String language) {
        if (vin == null) {
            throw new TermsInvalidInputException("Request missing vin");
        }
        if (countryCode == null) {
            throw new TermsInvalidInputException("Request missing country code");
        }
        if (language == null) {
            throw new TermsInvalidInputException("Request missing language");
        }
        Map<String, TermsDocVersion> versions = this.termsCache.getActiveCountryTermsHashes(countryCode, language);
        return findDecisions(vin, countryCode, versions);
    }

    private TermsAcceptance findDecisions(String vin, String countryCode, Map<String, TermsDocVersion> versions) {
        List<TermsDecisionRecord> records = this.termsDecisionRepo.findAllByVinAndHashIn(vin, versions.keySet());
        Map<String, TermsDecision> decisionMap = records.stream().collect(Collectors.toMap(TermsDecisionRecord::getHash, TermsDecisionRecord::getStatus));

        List<TermsDocVersion> accepted = new ArrayList<>();
        List<TermsDocVersion> rejected = new ArrayList<>();
        List<TermsDocVersion> pending = new ArrayList<>();

        versions.values().stream().forEach(vers -> {
            TermsDecision decision = decisionMap.get(vers.getHash());
            if (decision == null) {
                pending.add(vers);
            } else if (decision == TermsDecision.ACCEPT) {
                accepted.add(vers);
            } else {
                rejected.add(vers);
            }
        });

        TermsAcceptance acceptance = new TermsAcceptance();
        acceptance.setVin(vin);
        acceptance.setCountry(countryCode);
        acceptance.setAccepted(this.termsTranslator.translate(accepted));
        acceptance.setRejected(this.termsTranslator.translate(rejected));
        acceptance.setPending(this.termsTranslator.translate(pending));
        return acceptance;
    }

    private TermsAcceptance actionTerms(TermsRequest request, TermsDecision status) {
        if (request == null || request.getTermsHash() == null) {
            throw new TermsInvalidInputException("Request missing terms hashes");
        }
        if (request.getVin() == null) {
            throw new TermsInvalidInputException("Request missing vin");
        }
        if (request.getCountry() == null) {
            throw new TermsInvalidInputException("Request missing country");
        }

        ValidTermsList validTermsList = this.termsValidator.getValidAllTerms(request.getTermsHash());
        recordDecisions(request.getVin(), request.getCountry(), validTermsList.getValidHashes(), status);

        TermsAcceptance acceptance = new TermsAcceptance();
        acceptance.setVin(request.getVin());
        acceptance.setCountry(request.getCountry());
        if (status == TermsDecision.ACCEPT) {
            acceptance.setAccepted(this.termsTranslator.translate(validTermsList.getValidTerms()));
        } else {
            acceptance.setRejected(this.termsTranslator.translate(validTermsList.getValidTerms()));
        }
        acceptance.setInvalidHash(validTermsList.getInvalidHashes());
        return acceptance;
    }

    @Transactional
    private void recordDecisions(String vin, String country, List<String> hashList, TermsDecision status) throws TermsException {
        // Remove any old decisions for this vin/country/hashes
        this.termsDecisionRepo.deleteByVinAndHashIn(vin, hashList);

        // Add new decisions
        List<TermsDecisionRecord> decisions = new ArrayList<>();
        for (String hash : hashList) {
            TermsDecisionRecord decision = new TermsDecisionRecord(vin, hash, status);
            decisions.add(decision);
        }

        this.termsDecisionRepo.save(decisions);

        CountryIsoCode updatedCountry = this.vehiclesClient.updateVehicleCountry(vin, CountryIsoCode.valueOf(country));
        if (updatedCountry == null) {
            throw TermsExceptionCode.VEHICLES_UPDATE_FAILED.exception();
        }
    }

    public CountryIsoCode getCountryForVehicle(String vin) {
        return this.vehiclesClient.getVehicleCountry(vin);
    }

}
