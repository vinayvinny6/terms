package com.gm.gsmc.pix.terms.exception;

import org.springframework.http.HttpStatus;

/**
 * @author TZVZCY
 */
public enum TermsExceptionCode {

    DATABASE_FAILED("0001", "Database Connection Failed"), TERMS_FILE_EMPTY("0002", "Uploaded Terms File is empty"), TERMS_UPLOAD_FAILED("0003",
            "Upload Terms File Failed"), TERMS_FILE_NOT_FOUND("0004", "Terms File Not Found", HttpStatus.NOT_FOUND), VEHICLES_UPDATE_FAILED("0005",
                    "Updating Vehicles Service Failed"),;

    private static final String prefix = "TRMS";
    private final String code;
    private final String description;
    private final HttpStatus httpCode;

    private TermsExceptionCode (String code, String description) {
        this(code, description, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private TermsExceptionCode (String code, String description, HttpStatus httpCode) {
        this.code = prefix + "_" + code;
        this.description = description;
        this.httpCode = httpCode;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    public HttpStatus getHttpCode() {
        return this.httpCode;
    }

    public TermsException exception() {
        return new TermsException(this);
    }

}
