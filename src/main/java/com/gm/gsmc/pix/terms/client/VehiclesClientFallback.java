package com.gm.gsmc.pix.terms.client;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.gm.gsmc.pix.model.CountryIsoCode;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.model.vehicle.Vehicle;
import com.gm.gsmc.pix.model.vehicle.VehicleCapability;
import com.gm.gsmc.pix.model.vehicle.VehicleState;

/**
 * @author TZVZCY - Zac Grantham
 */
@Component
public class VehiclesClientFallback implements VehiclesClient {

    private static final Logger log = LoggerFactory.getLogger(VehiclesClientFallback.class);

    @Override
    public Vehicle createVehicle(Vehicle vehicle) {
        return null;
    }

    @Override
    public Vehicle getVehicle(String vin) {
        return null;
    }

    @Override
    public CountryIsoCode updateVehicleCountry(String vin, CountryIsoCode countryIsoCode) {
        log.info("Unable to update country for vin {} to {}", vin, countryIsoCode);
        return null;
    }

    @Override
    public CountryIsoCode getVehicleCountry(String vin) {
        return null;
    }

    @Override
    public List<VehicleCapability> getVehicleCapabilities(String vin) {
        return null;
    }

    @Override
    public List<SettingDescriptor> getDefaultVehicleSettings(String vin) {
        return null;
    }

    @Override
    public List<VehicleCapability> setVehicleCapabilities(String vin, List<VehicleCapability> vehicleCapabilities) {
        return null;
    }

    @Override
    public Vehicle updateVehicleState(String vin, VehicleState vehicleState) {
        return null;
    }

}
