package com.gm.gsmc.pix.terms.jpa;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gm.gsmc.pix.terms.jpa.model.TermsDecisionRecord;

/**
 * @author TZVZCY
 */
public interface TermsDecisionRecordRepository extends CrudRepository<TermsDecisionRecord, String> {

    public List<TermsDecisionRecord> findAllByVin(String vin);

    public List<TermsDecisionRecord> findAllByVinAndHashIn(String vin, Iterable<String> hash);

    public void deleteByVinAndHashIn(String vin, Iterable<String> hashList);

    public void deleteByVin(String vin);

//    @Query ("select rec from TermsDecisionRecord rec where rec.vin = :vin group by rec.hash having max(rec.decisionDate)")
//    public List<TermsDecisionRecord> getMostRecentDecisionsForVin(@Param ("vin") String vin);// , @Param ("hashList") List<String> hashList);
}
