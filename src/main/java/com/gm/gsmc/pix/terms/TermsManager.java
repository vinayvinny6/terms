package com.gm.gsmc.pix.terms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gm.gsmc.pix.api.service.PixException;
import com.gm.gsmc.pix.model.terms.Terms;
import com.gm.gsmc.pix.model.terms.TermsRequest;
import com.gm.gsmc.pix.model.terms.TermsSent;
import com.gm.gsmc.pix.model.terms.TermsType;
import com.gm.gsmc.pix.terms.email.TermsEmailer;
import com.gm.gsmc.pix.terms.exception.TermsInvalidInputException;
import com.gm.gsmc.pix.terms.jpa.TermsDocRepository;
import com.gm.gsmc.pix.terms.jpa.TermsDocVersionRepository;
import com.gm.gsmc.pix.terms.jpa.model.TermsDoc;
import com.gm.gsmc.pix.terms.jpa.model.TermsDocVersion;
import com.gm.gsmc.pix.terms.translator.TermsTranslator;

/**
 * @author TZVZCY
 */
@Service
public class TermsManager {

    @Autowired
    private TermsDocVersionRepository termsVersionRepo;

    @Autowired
    private TermsDocRepository termsDocRepo;

    @Autowired
    private TermsTranslator termsTranslator;

    @Autowired
    private TermsEmailer termsEmailer;

    @Autowired
    private TermsCache termsCache;

    public List<Terms> getAllTerms(String countryCode, String language) {
        if (countryCode == null) {
            throw new TermsInvalidInputException("Request missing country code");
        }
        if (language == null) {
            throw new TermsInvalidInputException("Request missing language");
        }
//        List<TermsDocVersion> termsDocs = this.termsVersionRepo.findAllByTermsDocCountryAndTermsDocLanguage(countryCode, language);
//        List<Terms> allTerms = this.termsTranslator.translate(termsDocs);
        return this.termsCache.getAllCountryTerms(countryCode, language);
    }

    public List<Terms> getActiveTerms(String countryCode, String language) {
        if (countryCode == null) {
            throw new TermsInvalidInputException("Request missing country code");
        }
        if (language == null) {
            throw new TermsInvalidInputException("Request missing language");
        }
//        List<TermsDocVersion> termsDocs = this.termsVersionRepo.getActiveVersions(countryCode, language);
//        List<Terms> allTerms = this.termsTranslator.translate(termsDocs);
        return this.termsCache.getActiveCountryTerms(countryCode, language);
    }

    public Terms getTerms(String hash) {
        if (hash == null) {
            throw new TermsInvalidInputException("Request missing hash");
        }
        TermsDocVersion termsDoc = this.termsVersionRepo.findOneByHash(hash);
        Terms term = this.termsTranslator.translate(termsDoc);
        return term;
    }

    public TermsSent emailTerms(String emailAddress, String countryCode, String language, TermsRequest request) throws PixException {
        return this.termsEmailer.emailTerms(emailAddress, countryCode, language, request);
    }

    public TermsDoc addTermsDoc(TermsDoc terms) {
        // TODO Add some verification
        return this.termsDocRepo.save(terms);
    }

    public TermsDocVersion addTermsVersion(TermsDocVersion terms) {
        // TODO Add some verification
        return this.termsVersionRepo.save(terms);
    }

    public TermsDoc findTermsDoc(TermsType type, String country, String language) {
        return this.termsDocRepo.findOneByCountryAndLanguageAndType(country, language, type);
    }

    public List<String> getCountryList() {
        return this.termsDocRepo.getCountries();
    }

    public List<String> getDocumentsInCountry(String country) {
        return this.termsDocRepo.getDocumentsInCountry(country);
    }

    public Map<String, List<Terms>> getDocumentsInCountry(String country, TermsType termsType) {
        Map<String, List<Terms>> docs = new HashMap<>();
        List<TermsDocVersion> versions = this.termsVersionRepo.findAllByTermsDocCountryAndTermsDocType(country, termsType);
        for (TermsDocVersion tdv : versions) {
            String language = tdv.getTermsDoc().getLanguage();
            List<Terms> terms = docs.get(language);
            if (terms == null) {
                terms = new ArrayList<>();
                docs.put(language, terms);
            }
            terms.add(this.termsTranslator.translate(tdv));
        }
        return docs;
    }

}
