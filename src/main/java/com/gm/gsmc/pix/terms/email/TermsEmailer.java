package com.gm.gsmc.pix.terms.email;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gm.gsmc.pix.model.terms.TermsRequest;
import com.gm.gsmc.pix.model.terms.TermsSent;
import com.gm.gsmc.pix.terms.TermsValidator;
import com.gm.gsmc.pix.terms.ValidTermsList;
import com.gm.gsmc.pix.terms.exception.TermsException;
import com.gm.gsmc.pix.terms.exception.TermsInvalidInputException;
import com.gm.gsmc.pix.terms.jpa.model.TermsDocVersion;
import com.gm.gsmc.pix.terms.translator.TermsTranslator;

/**
 * @author TZVZCY
 */
@Service
public class TermsEmailer {

    private static final Logger log = LoggerFactory.getLogger(TermsEmailer.class);

    @Autowired
    private TermsValidator termsValidator;

    @Autowired
    private TermsTranslator termsTranslator;

    public TermsSent emailTerms(String emailAddress, String countryCode, String language, TermsRequest request) throws TermsException, TermsInvalidInputException {
        if (request == null || request.getTermsHash() == null) {
            throw new TermsInvalidInputException("Request missing terms hashes");
        }
        if (request.getVin() == null) {
            throw new TermsInvalidInputException("Request missing vin");
        }
        if (emailAddress == null) {
            throw new TermsInvalidInputException("Request missing email address");
        }
        if (countryCode == null) {
            throw new TermsInvalidInputException("Request missing country code");
        }
        if (language == null) {
            throw new TermsInvalidInputException("Request missing language");
        }
        ValidTermsList validTermsList = this.termsValidator.getValidAllTerms(request.getTermsHash());
        List<TermsDocVersion> termsSentList = this.emailTerms(emailAddress, validTermsList.getValidTerms());

        TermsSent termsSent = new TermsSent();
        termsSent.setInvalidHash(validTermsList.getInvalidHashes());
        termsSent.setSent(this.termsTranslator.translate(termsSentList));
        return termsSent;
    }

    private List<TermsDocVersion> emailTerms(String emailAddress, List<TermsDocVersion> terms) {
        // TODO Send Email...
        log.info("Email to: {}, count: {}", emailAddress, terms.size());
        return terms;
    }

}
