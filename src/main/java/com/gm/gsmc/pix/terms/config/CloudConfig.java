package com.gm.gsmc.pix.terms.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.java.AbstractCloudConfig;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.gm.gsmc.pix.general.cache.CaffeineYamlConfiguration;
import com.gm.gsmc.pix.general.cache.ConfigurableCaffeineCacheManager;
import com.gm.gsmc.pix.general.s3.S3Service;
import com.gm.gsmc.pix.terms.client.VehiclesClient;

/**
 * @author TZVZCY
 */
@Configuration
@Profile ("cloud")
@EnableDiscoveryClient
@EnableFeignClients (basePackageClasses = VehiclesClient.class)
@EnableCircuitBreaker
@EnableCaching
public class CloudConfig extends AbstractCloudConfig {

    private static final Logger log = LoggerFactory.getLogger(CloudConfig.class);

    @Bean
    public S3Service getS3Service() {
        log.info("Loading Cloud Config - S3");
        return connectionFactory().service(S3Service.class);
    }

    @Bean
    public CaffeineYamlConfiguration getCaffeineYamlConfiguration() {
        return new CaffeineYamlConfiguration();
    }

    @Bean
    public CacheManager getCacheManager(CaffeineYamlConfiguration ymlConfig) {
        return new ConfigurableCaffeineCacheManager(ymlConfig);
    }

//    @Bean
//    public MailSender mailSender() {
//        return connectionFactory().service("mail-service", MailSender.class);
//    }
}
