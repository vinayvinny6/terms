package com.gm.gsmc.pix.terms.jpa.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.gm.gsmc.pix.model.terms.TermsStatus;

/**
 * @author TZVZCY
 */
@Entity
public class TermsDocVersion {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private TermsDoc termsDoc;

    private String url;
    private Date effectiveDate;
    private String hash;
    @Enumerated (EnumType.STRING)
    private TermsStatus status;
    private String version;
    @OneToOne
    private TermsFile file;

    public TermsDocVersion () {

    }

    public TermsDocVersion (TermsDoc termsDoc, TermsFile file, String url, Date effectiveDate, String hash, TermsStatus status, String version) {
        this.termsDoc = termsDoc;
        this.url = url;
        this.effectiveDate = effectiveDate;
        this.hash = hash;
        this.status = status;
        this.version = version;
        this.file = file;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TermsDoc getTermsDoc() {
        return this.termsDoc;
    }

    public void setTermsDoc(TermsDoc termsDoc) {
        this.termsDoc = termsDoc;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getEffectiveDate() {
        return this.effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public TermsStatus getStatus() {
        return this.status;
    }

    public void setStatus(TermsStatus status) {
        this.status = status;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public TermsFile getFile() {
        return this.file;
    }

    public void setFile(TermsFile file) {
        this.file = file;
    }

}
