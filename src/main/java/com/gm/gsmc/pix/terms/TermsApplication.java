package com.gm.gsmc.pix.terms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.cloudfoundry.CloudFoundryConnector;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author TZVZCY
 */
@SpringBootApplication
@EnableSwagger2
@ComponentScan (basePackages = "com.gm.gsmc.pix.terms")
//@EnableDiscoveryClient
//@EnableFeignClients
public class TermsApplication {

    private static final Logger log = LoggerFactory.getLogger(TermsApplication.class);

    public static void main(String[] args) {
        if (new CloudFoundryConnector().isInMatchingCloud()) {
            log.info("Terms - Setting profile to CLOUD");
            System.setProperty("spring.profiles.active", "cloud");
        }
        SpringApplication.run(TermsApplication.class, args);
    }
}
