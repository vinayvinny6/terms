package com.gm.gsmc.pix.terms.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gm.gsmc.pix.model.terms.TermsStatus;
import com.gm.gsmc.pix.model.terms.TermsType;
import com.gm.gsmc.pix.terms.jpa.model.TermsDoc;
import com.gm.gsmc.pix.terms.jpa.model.TermsDocVersion;

/**
 * @author TZVZCY
 */
public interface TermsDocVersionRepository extends CrudRepository<TermsDocVersion, Long> {

    @Query ("select tdv from TermsDocVersion tdv where tdv.termsDoc.country = :country and tdv.termsDoc.language = :language and tdv.status=com.gm.gsmc.pix.model.terms.TermsStatus.ACTIVE")
    public List<TermsDocVersion> getActiveVersions(@Param ("country") String country, @Param ("language") String language);

    public List<TermsDocVersion> findAllByTermsDocCountryAndTermsDocLanguage(String countryCode, String language);

    public List<TermsDocVersion> findAllByTermsDocCountryAndTermsDocType(String country, TermsType termsType);

    public List<TermsDocVersion> findAllByHashIn(List<String> hashList);

    public List<TermsDocVersion> findAllByStatus(TermsStatus status);

    public TermsDocVersion findOneByHash(String hash);

    public TermsDocVersion findOneByTermsDocAndVersion(TermsDoc termsDoc, String version);

}
