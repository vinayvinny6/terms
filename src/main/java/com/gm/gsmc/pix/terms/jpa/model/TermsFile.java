package com.gm.gsmc.pix.terms.jpa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author TZVZCY
 */
@Entity
public class TermsFile {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    private String hash;
    private String contentType;
    private String originalFileName;

    public TermsFile () {

    }

    public TermsFile (String hash, String contentType, String originalFileName) {
        this.hash = hash;
        this.contentType = contentType;
        this.originalFileName = originalFileName;
    }

    public String getFilename() {
        return "TERMS_" + this.id + "_" + this.hash;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getContentType() {
        return this.contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getOriginalFileName() {
        return this.originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

}
