package com.gm.gsmc.pix.terms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.gm.gsmc.pix.model.terms.Terms;
import com.gm.gsmc.pix.model.terms.TermsStatus;
import com.gm.gsmc.pix.model.terms.TermsType;
import com.gm.gsmc.pix.terms.TermsFileManager;

import io.swagger.annotations.Api;

/**
 * @author TZVZCY
 */
@RestController
@RequestMapping ("/manage")
@Api ("Terms Management Service - Manages Terms and Conditions files, status, uploads, etc. ")
public class TermsManagementController {

    @Autowired
    private TermsFileManager termsFileManager;

    @PostMapping (value = "/terms/create")
    public Terms createNewTerms(@RequestParam ("country") String country, @RequestParam ("language") String language,
            @RequestParam ("type") TermsType type, @RequestParam ("version") String version,
            @RequestParam ("effectiveDay") Integer effectiveDay,
            @RequestParam ("effectiveMonth") Integer effectiveMonth,
            @RequestParam ("effectiveYear") Integer effectiveYear,
            @RequestParam ("termsFile") MultipartFile termsFilePart) {
        return this.termsFileManager.createNewTerms(country, language, type, version, effectiveYear, effectiveMonth, effectiveDay, termsFilePart);
    }

    @GetMapping (value = "/terms/files/all")
    public List<String> listAllFiles() {
        return this.termsFileManager.listFiles();
    }

//    @DeleteMapping (value = "/terms/files/all")
//    public List<String> deleteAllFiles() {
//        return this.termsFileManager.deleteAllFiles();
//    }

    @PostMapping (value = "/terms/{hash}/status")
    public Terms updateStatus(@PathVariable (name = "hash") String termsHash, @RequestParam ("status") TermsStatus status) {
        return this.termsFileManager.setVersionStatus(termsHash, status);
    }

    @GetMapping (value = "/simulate/{crash}")
    public void simulateCrash(@PathVariable (name = "crash") String crash) {
        if ("crash".equalsIgnoreCase(crash)) {
            System.exit(1);
        }
    }

}
