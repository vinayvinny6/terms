package com.gm.gsmc.pix.terms.exception;

import com.gm.gsmc.pix.api.service.PixException;

/**
 * @author TZVZCY
 */
public class TermsException extends PixException {

    private static final long serialVersionUID = 8747706804640274765L;

    public TermsException (TermsExceptionCode code) {
        super(code.getHttpCode(), code.getDescription(), code.getCode());
    }

    public TermsException (TermsExceptionCode code, Throwable cause) {
        super(code.getHttpCode(), code.getDescription(), code.getCode(), cause);
    }

}
