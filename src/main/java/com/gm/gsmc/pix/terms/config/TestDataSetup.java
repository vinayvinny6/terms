package com.gm.gsmc.pix.terms.config;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.gm.gsmc.pix.general.setup.AbstractTestDataSetup;
import com.gm.gsmc.pix.model.terms.Terms;
import com.gm.gsmc.pix.model.terms.TermsStatus;
import com.gm.gsmc.pix.model.terms.TermsType;
import com.gm.gsmc.pix.terms.TermsFileManager;
import com.gm.gsmc.pix.terms.TermsManager;
import com.gm.gsmc.pix.terms.jpa.model.TermsDoc;

/**
 * @author TZVZCY
 */
@Component
public class TestDataSetup extends AbstractTestDataSetup {

    private static final Logger log = LoggerFactory.getLogger(TestDataSetup.class);

    @Autowired
    private TermsManager termsManager;

    @Autowired
    private TermsFileManager termsFileManager;

    @Override
    protected void setupTestData() {
        log.info("Creating Test Data");

        // Turn this back on if data is needed
        // buildAllTermsDocs();

    }

    private void buildAllTermsDocs() {
        createAllTermsDocsCombos(array("US"), array("en", "es"), array(TermsType.Privacy_Statement, TermsType.Terms_and_Conditions));
        createAllTermsDocsCombos(array("CA"), array("en", "fr"), array(TermsType.Privacy_Statement, TermsType.Terms_and_Conditions, TermsType.CASL));
        createAllTermsDocsCombos(array("MX"), array("en", "es"), array(TermsType.Privacy_Statement, TermsType.Terms_and_Conditions));
    }

    @SafeVarargs
    private final <T> T[] array(T... values) {
        return values;
    }

    private void createAllTermsDocsCombos(String[] countries, String[] languages, TermsType[] types) {
        for (String country : countries) {
            for (TermsType type : types) {
                List<Pair<Integer, TermsStatus>> versions = generateVersions();
                for (String language : languages) {
                    TermsDoc td = this.termsManager.addTermsDoc(new TermsDoc(country, language, type.getDescription(), type, "test user"));
                    createTermsVersions(td, versions);
                }
            }
        }
    }

    private List<Pair<Integer, TermsStatus>> generateVersions() {
        List<Pair<Integer, TermsStatus>> pairs = new ArrayList<>();
        int versionNumber = 1;

        // Create Expired Versions
        while (Math.random() > 0.5) {
            pairs.add(Pair.of(versionNumber, TermsStatus.EXPIRED));
            versionNumber++;
        }

        // Create Inactive Version
        if (Math.random() > 0.5) {
            pairs.add(Pair.of(versionNumber, TermsStatus.INACTIVE));
            versionNumber++;
        }

        pairs.add(Pair.of(versionNumber, TermsStatus.ACTIVE));
        versionNumber++;

        // Create Draft Versions
        while (Math.random() > 0.5) {
            pairs.add(Pair.of(versionNumber, TermsStatus.DRAFT));
            versionNumber++;
        }
        return pairs;
    }

    private void createTermsVersions(TermsDoc td, List<Pair<Integer, TermsStatus>> pairs) {
        for (Pair<Integer, TermsStatus> pair : pairs) {
            createVersion(td, pair.getFirst(), pair.getSecond());
        }
    }

    private void createVersion(TermsDoc td, int versionNumber, TermsStatus status) {
        String versionNumString = versionNumber + ".0";
        String fileName = buildFileName(td, versionNumString);
        String testContents = createHtmlDocument(td, versionNumString);
        MultipartFile termsFilePart = new TestMultipartFile(fileName, MediaType.TEXT_HTML_VALUE, testContents.getBytes());
        Terms terms = this.termsFileManager.createNewTerms(td.getCountry(), td.getLanguage(), td.getType(), versionNumString, 2017, versionNumber, 1, termsFilePart);
        this.termsFileManager.setVersionStatus(terms.getCountry(), terms.getLanguage(), terms.getType(), terms.getVersion(), status);
    }

    private String createHtmlDocument(TermsDoc td, String versionNum) {
        String bigName = td.getDisplayName() + " " + td.getCountry() + "-" + td.getLanguage();
        String lorem =
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        while (Math.random() > 0.7) {
            lorem = lorem + lorem;
        }
        return "<html><head><title>" + bigName + "</title></head><body><h3>" + bigName + "</h3><br/><h4>Version: " + versionNum + "</h4><br/>" + lorem + "</body></html>";
    }

    private String buildFileName(TermsDoc td, String version) {
        return td.getType().getDescription() + "_" + td.getLanguage() + "-" + td.getCountry() + "_" + version + ".html";
    }

    static private class TestMultipartFile implements MultipartFile {

        private final String name;
        private String originalFilename;
        private String contentType;
        private final byte[] content;

        public TestMultipartFile (String name, String originalFilename, String contentType, byte[] content) {
            this.name = name;
            this.originalFilename = (originalFilename != null ? originalFilename : "");
            this.contentType = contentType;
            this.content = (content != null ? content : new byte[0]);
        }

        public TestMultipartFile (String name, String contentType, byte[] content) {
            this(name, name, contentType, content);
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public String getOriginalFilename() {
            return this.originalFilename;
        }

        @Override
        public String getContentType() {
            return this.contentType;
        }

        @Override
        public boolean isEmpty() {
            return (this.content.length == 0);
        }

        @Override
        public long getSize() {
            return this.content.length;
        }

        @Override
        public byte[] getBytes() throws IOException {
            return this.content;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return new ByteArrayInputStream(this.content);
        }

        @Override
        public void transferTo(File dest) throws IOException, IllegalStateException {
            FileCopyUtils.copy(this.content, dest);
        }

    }

}
