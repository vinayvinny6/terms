package com.gm.gsmc.pix.terms.exception;

import org.springframework.http.HttpStatus;

/**
 * @author TZVZCY
 */
public class TermsInvalidInputException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final String description;
    private final HttpStatus httpCode;

    public TermsInvalidInputException (String description) {
        this(description, HttpStatus.BAD_REQUEST);
    }

    public TermsInvalidInputException (String description, HttpStatus httpCode) {
        this.description = description;
        this.httpCode = httpCode;
    }

    public String getDescription() {
        return this.description;
    }

    public HttpStatus getHttpCode() {
        return this.httpCode;
    }

}
