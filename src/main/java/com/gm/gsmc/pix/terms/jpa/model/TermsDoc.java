package com.gm.gsmc.pix.terms.jpa.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.gm.gsmc.pix.model.terms.TermsType;

/**
 * @author TZVZCY
 */
@Entity
public class TermsDoc {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    private String country;
    private String language;
    private String displayName;

    @Enumerated (EnumType.STRING)
    private TermsType type;
    private String owner;

    public TermsDoc () {

    }

    public TermsDoc (String country, String language, String displayName, TermsType type, String owner) {
        this.country = country;
        this.language = language;
        this.displayName = displayName;
        this.type = type;
        this.owner = owner;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public TermsType getType() {
        return this.type;
    }

    public void setType(TermsType type) {
        this.type = type;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

}
