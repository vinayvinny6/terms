package com.gm.gsmc.pix.terms;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.emc.object.s3.bean.GetObjectResult;
import com.emc.object.s3.bean.PutObjectResult;
import com.gm.gsmc.pix.general.s3.S3Service;
import com.gm.gsmc.pix.model.terms.Terms;
import com.gm.gsmc.pix.model.terms.TermsStatus;
import com.gm.gsmc.pix.model.terms.TermsType;
import com.gm.gsmc.pix.terms.exception.TermsException;
import com.gm.gsmc.pix.terms.exception.TermsExceptionCode;
import com.gm.gsmc.pix.terms.jpa.TermsDocRepository;
import com.gm.gsmc.pix.terms.jpa.TermsDocVersionRepository;
import com.gm.gsmc.pix.terms.jpa.TermsFileRepository;
import com.gm.gsmc.pix.terms.jpa.model.TermsDoc;
import com.gm.gsmc.pix.terms.jpa.model.TermsDocVersion;
import com.gm.gsmc.pix.terms.jpa.model.TermsFile;
import com.gm.gsmc.pix.terms.translator.TermsTranslator;
import com.google.common.hash.Hashing;

/**
 * @author TZVZCY
 */
@Service
public class TermsFileManager {

    // *********************
    // In SFDC, the has value is the SHA-256 hash of the file, plus the ID of the agreement
    // In new system, we will migrate over old agreements (with their ID), but new agreements will use just the hash of the file
    // *********************

    private static final Logger log = LoggerFactory.getLogger(TermsFileManager.class);

    @Autowired
    private S3Service s3Service;

    @Autowired
    private TermsFileRepository termsFileRepo;

    @Autowired
    private TermsDocVersionRepository termsVersionRepo;

    @Autowired
    private TermsDocRepository termsDocRepo;

    @Autowired
    private TermsTranslator termsTranslator;

    @Value ("${vcap.application.uris[0]:localhost:8080}")
    private String baseUrl;

    public GetObjectResult<InputStream> readFile(String termsName) {
        return this.s3Service.readFile(termsName);
    }

    public GetObjectResult<InputStream> readFile(Long id, String hash) {
        TermsFile file = this.termsFileRepo.findOneByIdAndHash(id, hash);
        if (file == null) {
            throw TermsExceptionCode.TERMS_FILE_NOT_FOUND.exception();
        }
        return readFile(file.getFilename());
    }

    public void deleteFile(String termsName) {
        this.s3Service.deleteFile(termsName);
    }

    public List<String> listFiles() {
        return this.s3Service.listFiles().stream().filter(name -> name.startsWith("TERMS_")).collect(Collectors.toList());
    }

    public List<String> deleteAllFiles() {
        List<String> files = this.s3Service.listFiles();
        for (String file : files) {
            this.s3Service.deleteFile(file);
        }
        return files;
    }

    private String hashFile(byte[] file) {
        String sha256hex = Hashing.sha256().hashBytes(file).toString();
        return sha256hex;
    }

    @Transactional
    public Terms createNewTerms(String country, String language, TermsType type, String version, Date effectiveDate, MultipartFile termsFilePart) {
        if (termsFilePart.isEmpty()) {
            throw new TermsException(TermsExceptionCode.TERMS_FILE_EMPTY);
        }

        // Get Terms Doc
        TermsDoc termsDoc = this.termsDocRepo.findOneByCountryAndLanguageAndType(country, language, type);

        // Create Terms File
        TermsFile termsFile = createTermsFile(termsFilePart);
        termsFile = this.termsFileRepo.save(termsFile);

        // Create Terms Doc Version
        TermsDocVersion docVersion = new TermsDocVersion(termsDoc, termsFile, buildUrl(termsFile), effectiveDate, termsFile.getHash(), TermsStatus.DRAFT, version);
        this.termsVersionRepo.save(docVersion);

        // Upload File
        PutObjectResult result = null;
        try {
            result = this.s3Service.putFile(termsFile.getFilename(), termsFile.getContentType(), termsFilePart.getInputStream());
        } catch (IOException ex) {
            log.error("File Upload Failed", ex);
            // throw new TermsException(TermsExceptionCode.TERMS_UPLOAD_FAILED);
            throw TermsExceptionCode.TERMS_UPLOAD_FAILED.exception();
        }
        if (result == null) {
            throw new TermsException(TermsExceptionCode.TERMS_UPLOAD_FAILED);
        }

        // Return the terms Doc
        return this.termsTranslator.translate(docVersion);
    }

    private String buildUrl(TermsFile termsFile) {
        return "http://" + this.baseUrl + "/file/download/" + termsFile.getId() + "/" + termsFile.getHash() + "/" + termsFile.getOriginalFileName();
    }

    private TermsFile createTermsFile(MultipartFile termsFilePart) {
        String hash;
        try {
            hash = hashFile(termsFilePart.getBytes());
        } catch (IOException ex) {
            throw new TermsException(TermsExceptionCode.TERMS_UPLOAD_FAILED);
        }

        String contentType = termsFilePart.getContentType();
        if (contentType == null) {
            contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE;
        }

        String originalFileName = termsFilePart.getOriginalFilename();
        if (originalFileName == null) {
            // pick a generic filename
            originalFileName = "terms";
        } else {
            // Trim off the path if it exists
            originalFileName = originalFileName.substring(originalFileName.lastIndexOf("/") + 1).substring(originalFileName.lastIndexOf("\\") + 1);
        }
        TermsFile termsFile = new TermsFile(hash, contentType, originalFileName);
        return termsFile;
    }

    public Terms setVersionStatus(String country, String language, TermsType type, String version, TermsStatus status) {
        TermsDoc termsDoc = this.termsDocRepo.findOneByCountryAndLanguageAndType(country, language, type);
        TermsDocVersion tdv = this.termsVersionRepo.findOneByTermsDocAndVersion(termsDoc, version);
        tdv = setVersionStatus(tdv, status);
        return this.termsTranslator.translate(tdv);
    }

    public Terms setVersionStatus(String hash, TermsStatus status) {
        TermsDocVersion tdv = this.termsVersionRepo.findOneByHash(hash);
        tdv = setVersionStatus(tdv, status);
        return this.termsTranslator.translate(tdv);
    }

    private TermsDocVersion setVersionStatus(TermsDocVersion tdv, TermsStatus status) {
        tdv.setStatus(status);
        this.termsVersionRepo.save(tdv);
        return tdv;
    }

    public Terms createNewTerms(String country, String language, TermsType type, String version, Integer effectiveYear, Integer effectiveMonth, Integer effectiveDay, MultipartFile termsFilePart) {
        GregorianCalendar cal = new GregorianCalendar(effectiveYear, effectiveMonth - 1, effectiveDay);
        return createNewTerms(country, language, type, version, cal.getTime(), termsFilePart);
    }

}
